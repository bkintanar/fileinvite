import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  items: null
}

// getters
export const getters = {
  items: state => state.items
}

// mutations
export const mutations = {

  [types.FETCH_ITEMS_SUCCESS](state, { items }) {
    state.items = items
  },

  [types.FETCH_ITEMS_FAILURE](state) {
    state.items = null
  }
}

// actions
export const actions = {

  async fetchItems({ commit }) {
    try {
      const { data: { data } } = await axios.get('/api/items')

      commit(types.FETCH_ITEMS_SUCCESS, { items: data })
    } catch (e) {
      commit(types.FETCH_ITEMS_FAILURE)
    }
  },

  async fetchItem({ commit }, id) {
    try {
      await axios.get(`/api/items/${id}`)
    } catch (e) { }
  },

  async updateItem({ commit }, item) {

    const is_completed = !item.completed_at ? true : false;
    try {
      await axios.patch(`/api/items/${item.id}`, { is_completed })
    } catch (e) { }
  },

  async deleteItem({ commit }, item) {

    try {
      await axios.delete(`/api/items/${item.id}`)

    } catch (e) { }
  }
}
