<?php

use App\Eloquent\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            [
                'id'                => 1,
                'name'              => 'Bertrand Kintanar',
                'email'             => 'bertrand.kintanar@gmail.com',
                'email_verified_at' => now(),
                'password'          => '$2y$10$UUGi2hFUgzslIEt/i57Q6uCQ4eHgn4Ns4b6cthVKX0tLosJjduoku',
                'remember_token'    => 'OJPEKgmVW9',
                'created_at'        => now(),
                'updated_at'        => now(),
            ],
        ]);

        factory(User::class, 50)->create();
    }
}
