<?php

namespace Tests\Auth;

use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;

class RegisterTest extends TestCase
{
    /** @test */
    public function can_register_with_correct_parameters()
    {
        $suffix = rand(0, 100000);
        $data = [
            'name'                  => 'Test Case',
            'email'                 => "bertrand.kintanar+{$suffix}@gmail.com",
            'password'              => 'password',
            'password_confirmation' => 'password',
        ];

        $response = $this->json('POST', 'api/register', $data);

        $response->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(['name', 'email', 'updated_at', 'created_at', 'id']);
    }

    /** @test */
    public function cannot_register_if_name_is_missing()
    {
        $suffix = rand(0, 100000);
        $data = [
            'email'                 => "bertrand.kintanar+{$suffix}@gmail.com",
            'password'              => 'password',
            'password_confirmation' => 'password',
        ];

        $response = $this->json('POST', 'api/register', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure(['message', 'errors' => ['name']]);

        $data = $response->getData();

        $error = data_get($data, 'errors.name.0');

        $this->assertEquals($error, trans('validation.required', ['attribute' => 'name']));
    }

    /** @test */
    public function cannot_register_if_email_is_missing()
    {
        $data = [
            'name'                  => 'Test Case',
            'password'              => 'password',
            'password_confirmation' => 'password',
        ];

        $response = $this->json('POST', 'api/register', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure(['message', 'errors' => ['email']]);

        $data = $response->getData();

        $error = data_get($data, 'errors.email.0');

        $this->assertEquals($error, trans('validation.required', ['attribute' => 'email']));
    }

    /** @test */
    public function cannot_register_if_password_is_missing()
    {
        $suffix = rand(0, 100000);
        $data = [
            'name'                  => 'Test Case',
            'email'                 => "bertrand.kintanar+{$suffix}@gmail.com",
        ];

        $response = $this->json('POST', 'api/register', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure(['message', 'errors' => ['password']]);

        $data = $response->getData();

        $error = data_get($data, 'errors.password.0');

        $this->assertEquals($error, trans('validation.required', ['attribute' => 'password']));
    }

    /** @test */
    public function cannot_register_if_password_confirmation_is_missing()
    {
        $suffix = rand(0, 100000);
        $data = [
            'name'                  => 'Test Case',
            'email'                 => "bertrand.kintanar+{$suffix}@gmail.com",
            'password'              => 'password',
        ];

        $response = $this->json('POST', 'api/register', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure(['message', 'errors' => ['password']]);

        $data = $response->getData();

        $error = data_get($data, 'errors.password.0');

        $this->assertEquals($error, trans('validation.confirmed', ['attribute' => 'password']));
    }
}
