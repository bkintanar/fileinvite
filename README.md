## Steps

1. Copy .env.example to .env
2. Modify the DB setting
3. Run `sh ./scripts/setup.sh`
4. Run `php artisan serve`
5. Open another terminal and run `npm run watch`
6. Visit http://localhost:8000

## Authentication

You can either use the credentials provided below or register a new user:

```json
{
    'email': bertrand.kintanar@gmail.com,
    'password': password
}
```

## Postman

API Documentation can be found in postman:

https://documenter.getpostman.com/view/78990/SW7bzma8?version=latest

## Test coverage
You can find the test coverage in `./build/coverage/index.html`
