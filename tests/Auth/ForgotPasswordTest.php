<?php

namespace Tests\Auth;

use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;

class ForgotPasswordTest extends TestCase
{
    /** @test */
    public function can_send_reset_link_to_email()
    {
        $data = ['email' => 'bertrand.kintanar@gmail.com'];

        $response = $this->json('POST', 'api/forgot-password', $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(['status']);

        $data = $response->getData();

        $status = data_get($data, 'status');

        $this->assertEquals($status, trans('passwords.sent'));
    }

    /** @test */
    public function cannot_send_reset_link_to_unknown_email()
    {
        $data = ['email' => 'unknown.email@gmail.com'];

        $response = $this->json('POST', 'api/forgot-password', $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(['email']);

        $data = $response->getData();

        $email = data_get($data, 'email');

        $this->assertEquals($email, trans('passwords.user'));
    }
}
