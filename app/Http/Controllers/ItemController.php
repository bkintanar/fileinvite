<?php

namespace App\Http\Controllers;

use App\Eloquent\Item;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\ItemRequest;
use App\Http\Resources\ItemResource;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ItemRequest $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(ItemRequest $request): AnonymousResourceCollection
    {
        $perPage = $request->per_page ?? self::ITEMS_PER_PAGE;

        $user = $request->user();

        $items = $this->paginate($user->items, $perPage, $request->page, ['path' => $request->fullUrl()]);

        return ItemResource::collection($items);
    }

    /**
     * Display the specified resource.
     *
     * @param ItemRequest $request
     * @param Item        $item
     *
     * @return ItemResource
     */
    public function show(ItemRequest $request, Item $item): ItemResource
    {
        return new ItemResource($item);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ItemRequest $request
     *
     * @return ItemResource
     */
    public function store(ItemRequest $request): ItemResource
    {
        $user = $request->user();
        
        $data = ['name' => $request->name];
        
        $item = Item::create($data);

        $user->items()->save($item);

        return new ItemResource($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ItemRequest $request
     * @param Item        $item
     *
     * @return ItemResource
     */
    public function update(ItemRequest $request, Item $item): ItemResource
    {
        $data = $request->only(['user_id', 'name']);

        $data['completed_at'] = $request->is_completed ? now() : null;
        
        $item = tap($item)->update($data);

        return new ItemResource($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ItemRequest $request
     * @param Item        $item
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(ItemRequest $request, Item $item): JsonResponse
    {
        $item->delete();

        return response()->json(['status' => trans('fileinvite.delete_successful')], Response::HTTP_OK);
    }
}
