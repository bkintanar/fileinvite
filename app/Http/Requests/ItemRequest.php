<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    protected $rules = [
        'GET' => [],
        'POST' => [
            'name' => 'required',
        ],
        'PATCH' => [
            'name' => 'filled',
        ],
        'DELETE' => [],
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->item && $user = $this->user()) {
            return $user->items->contains($this->item);
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules[$this->method()];
    }
}
