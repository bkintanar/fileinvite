#!/bin/sh

composer install
npm install

php artisan migrate:fresh --seed
php artisan passport:install --force
./vendor/bin/phpunit
