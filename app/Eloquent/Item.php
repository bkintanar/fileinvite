<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use LogsActivity, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'completed_at', 'created_at', 'updated_at', 'deleted_at'];

    protected static $logAttributes = ['name', 'completed_at'];
}
