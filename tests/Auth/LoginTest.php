<?php

namespace Tests\Auth;

use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;

class LoginTest extends TestCase
{
    /** @test */
    public function can_login_using_correct_credentials()
    {
        $data = [
            'email'    => 'bertrand.kintanar@gmail.com',
            'password' => 'password',
        ];

        $response = $this->json('POST', 'api/login', $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(['token_type', 'expires_at', 'token']);

        $data = $response->getData();

        $this->token = $data->token;
    }

    /** @test */
    public function cannot_login_using_wrong_credentials()
    {
        $data = [
            'email'    => 'thisemailisinvalid',
            'password' => 'password',
        ];

        $response = $this->json('POST', 'api/login', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure(['message', 'errors' => ['email']]);

        $data = $response->getData();

        $error = data_get($data, 'errors.email.0');

        $this->assertEquals($error, trans('auth.failed'));
    }

    /** @test */
    public function cannot_login_if_email_is_missing()
    {
        $data = [
            'password' => 'password',
        ];

        $response = $this->json('POST', 'api/login', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure(['message', 'errors' => ['email']]);

        $data = $response->getData();

        $error = data_get($data, 'errors.email.0');

        $this->assertEquals($error, trans('validation.required', ['attribute' => 'email']));
    }

    /** @test */
    public function cannot_login_if_password_is_missing()
    {
        $data = [
            'email' => 'bertrand.kintanar@gmail.com',
        ];

        $response = $this->json('POST', 'api/login', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure(['message', 'errors' => ['password']]);

        $data = $response->getData();

        $error = data_get($data, 'errors.password.0');

        $this->assertEquals($error, trans('validation.required', ['attribute' => 'password']));
    }

    /** @test */
    public function cannot_login_if_no_parameter_passed()
    {
        $data = [];

        $response = $this->json('POST', 'api/login', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure(['message', 'errors' => ['email', 'password']]);

        $data = $response->getData();

        $passwordError = data_get($data, 'errors.password.0');
        $emailError = data_get($data, 'errors.email.0');

        $this->assertEquals($passwordError, trans('validation.required', ['attribute' => 'password']));
        $this->assertEquals($emailError, trans('validation.required', ['attribute' => 'email']));
    }

    /** @test */
    public function can_logout_using_correct_authentication_header()
    {
        $this->can_login_using_correct_credentials();

        $response = $this->json('POST', 'api/logout', [], ['Authorization' => 'Bearer ' . $this->token]);

        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function can_logout_when_not_authenticated()
    {
        $response = $this->json('POST', 'api/logout', [], ['Authorization' => 'Bearer ' . $this->token]);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
