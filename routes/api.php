<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// guest
Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');                                             // postman
    Route::post('register', 'Auth\RegisterController@register')->name('register.store');            // postman
    Route::post('forgot-password', 'Auth\ForgotPasswordController@sendResetLinkEmail');             // postman
    Route::post('reset-password', 'Auth\ResetPasswordController@reset')->name('password.reset');    // postman
});

// auth
Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/user', function (Request $request) {                                               // postman
        return $request->user();
    });

    Route::get('items', 'ItemController@index')->name('item.index');                                // postman
    Route::get('items/{item}', 'ItemController@show')->name('item.show');                           // postman
    Route::post('items', 'ItemController@store')->name('item.store');                               // postman
    Route::patch('items/{item}', 'ItemController@update')->name('item.update');                     // postman
    Route::delete('items/{item}', 'ItemController@destroy')->name('item.destroy');                  // postman
    
    Route::post('logout', 'Auth\LoginController@logout');                                           // postman
});
