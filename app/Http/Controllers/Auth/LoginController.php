<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $tokenResult;

    /**
     * Attempt to log the user into the application.
     *
     * @param Request $request
     *
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $attempt = $this->guard()->attempt($this->credentials($request));

        if ($attempt) {
            $user = $this->guard()->user();

            $token = $user->createToken('App');

            $this->tokenResult = $token;

            return true;
        }

        return false;
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);

        $token = (string)$this->tokenResult->accessToken;

        return response()->json([
            'token'      => $token,
            'token_type' => 'bearer',
            'expires_at' => Carbon::parse($this->tokenResult->token->expires_at)->toDateTimeString(),
        ], Response::HTTP_OK);
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     *
     * @return void
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json(['logout' => true], Response::HTTP_OK);
    }
}
