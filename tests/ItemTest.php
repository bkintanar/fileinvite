<?php

namespace Tests;

use App\Eloquent\Item;
use Symfony\Component\HttpFoundation\Response;

class ItemTest extends TestCase
{
    /** @test */
    public function can_add_item()
    {
        $data = [
            'name' => 'item 1',
        ];

        $response = $this->authApi('POST', 'api/items', $data);

        $response->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'user_id',
                    'name',
                    'completed_at',
                    'created_at',
                    'updated_at',
                ],
            ]);
    }

    /** @test */
    public function can_retrieve_items_paginated()
    {
        $response = $this->authApi('GET', 'api/items');

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'user_id',
                        'name',
                        'completed_at',
                        'created_at',
                        'updated_at',
                    ],
                ],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function can_retrieve_single_item()
    {
        $item = Item::where('user_id', 1)->first();

        $response = $this->authApi('GET', "api/items/{$item->id}");

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'user_id',
                    'name',
                    'completed_at',
                    'created_at',
                    'updated_at',
                ],
            ]);
    }

    /** @test */
    public function cannot_add_item_if_not_authenticated()
    {
        $data = [
            'name' => 'item 1',
        ];

        $response = $this->json('POST', 'api/items', $data);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJsonStructure(['message']);
    }

    /** @test */
    public function can_mark_item_complete()
    {
        $data = [
            'is_completed' => true,
        ];

        $item = Item::where('user_id', 1)->whereNull('completed_at')->first();

        $response = $this->authApi('PATCH', "api/items/{$item->id}", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'user_id',
                    'name',
                    'completed_at',
                    'created_at',
                    'updated_at',
                ],
            ]);

        $updatedItem = Item::find($item->id);

        $this->assertNotNull($updatedItem->completed_at);
    }

    /** @test */
    public function can_mark_item_incomplete()
    {
        $data = [
            'is_completed' => false,
        ];

        $item = Item::where('user_id', 1)->whereNotNull('completed_at')->first();

        $response = $this->authApi('PATCH', "api/items/{$item->id}", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'user_id',
                    'name',
                    'completed_at',
                    'created_at',
                    'updated_at',
                ],
            ]);

        $updatedItem = Item::find($item->id);

        $this->assertNull($updatedItem->completed_at);
    }

    /** @test */
    public function can_update_item_name()
    {
        $data = [
            'name' => 'new!!',
        ];

        $item = Item::where('user_id', 1)->first();

        $response = $this->authApi('PATCH', "api/items/{$item->id}", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'user_id',
                    'name',
                    'completed_at',
                    'created_at',
                    'updated_at',
                ],
            ]);

        $updatedItem = Item::find($item->id);

        $this->assertEquals($updatedItem->name, 'new!!');
    }

    /** @test */
    public function can_delete_item()
    {
        $item = Item::where('user_id', 1)->latest()->first();

        $response = $this->authApi('DELETE', "api/items/{$item->id}");

        $response->assertStatus(Response::HTTP_OK)->assertJsonStructure(['status']);

        $data = $response->getData();

        $this->assertEquals(trans($data->status), trans('fileinvite.delete_successful'));
    }
}
